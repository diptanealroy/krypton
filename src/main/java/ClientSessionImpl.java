import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;


public class ClientSessionImpl implements Application {
    private static final Logger log = LoggerFactory.getLogger(ClientSessionImpl.class);
    private SessionID sessionID = null;

    public SessionID getSessionID() {
        return sessionID;
    }

    @Override
    public void onCreate(SessionID sessionId) {
        log.info("ClientSessionImpl: onCreate callback");
        this.sessionID = sessionId;
    }

    @Override
    public void onLogon(SessionID sessionId) {
        log.info("Client - onLogon callback");
    }

    @Override
    public void onLogout(SessionID sessionId) {
        log.info("Client - onLogout callback");
    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {
        log.info("Client - toAdmin callback :" + message.toRawString());
    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {
        log.info("Client - fromAdmin callback :" + message.toRawString());
    }

    @Override
    public void toApp(Message message, SessionID sessionId) throws DoNotSend {
        log.info("Client - toApp callback :" + message.toRawString());
    }

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        log.info("Client - fromApp callback :" + message.toRawString());
    }
}
