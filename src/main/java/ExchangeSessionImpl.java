import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;


public class ExchangeSessionImpl implements Application {
    private static final Logger log = LoggerFactory.getLogger(ExchangeSessionImpl.class);

    private SessionID sessionID = null;

    ExchangeSessionImpl() throws ConfigError {
    }

    public SessionID getSessionID() {
        return sessionID;
    }

    @Override
    public void onCreate(SessionID sessionId) {
        log.info("ExchangeSessionImpl: onCreate callback");
        this.sessionID = sessionId;
    }

    @Override
    public void onLogon(SessionID sessionId) {
        log.info("onLogon callback");

    }

    @Override
    public void onLogout(SessionID sessionId) {
        log.info("onLogout callback");

    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {
        log.info("toAdmin callback");

    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {
        log.info("fromAdmin callback");

    }

    @Override
    public void toApp(Message message, SessionID sessionId) throws DoNotSend {
        log.info("toApp callback");

    }

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        log.info("fromApp callback");

    }
}
