package client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;

public class ClientSession extends MessageCracker implements Application {
    private static final Logger log = LoggerFactory.getLogger(ClientSession.class);
    private SessionID sessionId = null;

    public SessionID getSessionID() {
        return sessionId;
    }

    @Override
    public void onCreate(SessionID sessionId) {
        log.info("ClientSession: onCreate callback");
        this.sessionId = sessionId;
    }

    @Override
    public void onLogon(SessionID sessionId) {
        log.info("ClientSession: onLogon callback");
    }

    @Override
    public void onLogout(SessionID sessionId) {
        log.info("ClientSession: onLogout callback");
    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {
        log.info("toAdmin :" + message.toString());

    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {
        log.info("fromAdmin :" + message.toString());
    }

    @Override
    public void toApp(Message message, SessionID sessionId) throws DoNotSend {
        log.info("Outbound message :" + message.toString());
    }

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        log.info("Inbound message :" + message.toString());
    }
}
