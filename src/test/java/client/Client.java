package client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;

import java.util.List;

public class Client implements InSession {
    private static final Logger log = LoggerFactory.getLogger(Client.class);

    private final SocketInitiator initiator;
    private ClientSession session = null;

    public Client() throws ConfigError, FieldConvertError {
        SessionSettings settings = new SessionSettings("initiator.config");
        session = new ClientSession();
        MessageStoreFactory messageStoreFactory = new FileStoreFactory(settings);
        LogFactory logFactory = new ScreenLogFactory(true, true, true);
        MessageFactory messageFactory = new DefaultMessageFactory();

        initiator = new SocketInitiator(session, messageStoreFactory, settings, logFactory,
                messageFactory);
        initiator.start();
        log.info("Client application ready to connect to server:" + initiator.toString());
    }

    public void stop() throws RuntimeException, ConfigError {
        initiator.stop();
    }

    public boolean isLoggedOn() {
        return initiator.isLoggedOn();
    }

    public List<SessionID> getSessions() {
        return initiator.getSessions();
    }

    @Override
    public SessionID getSessionID() {
        return session.getSessionID();
    }
}
