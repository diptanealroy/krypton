package client;

import quickfix.SessionID;

public interface InSession {
    SessionID getSessionID();
}
