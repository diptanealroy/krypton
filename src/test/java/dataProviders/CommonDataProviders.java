package dataProviders;

import org.testng.annotations.DataProvider;

public class CommonDataProviders {

    @DataProvider(name="orderType")
    public Object[][] orderTypeData() {
        return new Object[][] {
                {'1'}, {'2'}, {'7'}
        };
    }

    @DataProvider(name="side")
    public Object[][] sideData() {
        return new Object[][] {
                {'1'}, {'2'}, {'5'}
        };
    }

    @DataProvider(name="symbol")
    public Object[][] symbolDataProvider() {
        return new Object[][] {
                {"TCS.NS"}, {"INFY.NS"}, {"HCLTECH.NS"}
        };
    }


}
