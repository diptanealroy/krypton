package common;

import client.Client;
import client.InSession;
import com.google.inject.AbstractModule;
import exchange.Exchange;
import exchange.OutSession;

public class SessionsModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(InSession.class).to(Client.class);
        bind(OutSession.class).to(Exchange.class);
    }
}
