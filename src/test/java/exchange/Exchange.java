package exchange;

import client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;

import java.io.IOException;
import java.util.List;

public class Exchange implements OutSession {
    private static final Logger log = LoggerFactory.getLogger(Exchange.class);
    private final SocketAcceptor acceptor;
    private ExchangeSession session = null;

    public Exchange() throws ConfigError, FieldConvertError {
        SessionSettings settings = new SessionSettings("acceptor.config");
        session = new ExchangeSession();
        MessageStoreFactory messageStoreFactory = new FileStoreFactory(settings);
        LogFactory logFactory = new ScreenLogFactory(true, true, true);
        MessageFactory messageFactory = new DefaultMessageFactory();

        acceptor = new SocketAcceptor(session, messageStoreFactory, settings, logFactory,
                messageFactory);
        acceptor.start();
        log.info("Server ready to accept connection");
    }

    public SessionID getSessionID() {
        return session.getSessionID();
    }


    public void stop() throws RuntimeException, ConfigError {
        acceptor.stop();
    }

    public boolean isLoggedOn() {
        return acceptor.isLoggedOn();
    }

    public List<SessionID> getSessions() {
        return acceptor.getSessions();
    }

}
