package exchange;

import quickfix.SessionID;

public interface OutSession {
    SessionID getSessionID();
}
