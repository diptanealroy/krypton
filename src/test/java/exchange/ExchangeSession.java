package exchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;
import quickfix.field.*;

public class ExchangeSession extends MessageCracker implements Application {
    private static final Logger log = LoggerFactory.getLogger(ExchangeSession.class);
    private SessionID sessionId = null;

    private int m_orderID = 0;
    private int m_execID = 0;

    public SessionID getSessionID() {
        return sessionId;
    }

    @Override
    public void onCreate(SessionID sessionId) {
        log.info("ExchangeSession onCreate callback received :" + sessionId.toString());
        this.sessionId = sessionId;
    }

    @Override
    public void onLogon(SessionID sessionId) {
        log.info("ExchangeSession Logon callback received");
    }

    @Override
    public void onLogout(SessionID sessionId) {
        log.info("Logout callback received");
    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {
        log.info("Exchange - ToAdmin callback received :" + message.toString());
    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {
        log.info("Exchange - FromAdmin callback received :" + message.toString());

    }

    @Override
    public void toApp(Message message, SessionID sessionId) throws DoNotSend {
        log.info("Inbound message :" + message.toString());
    }

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        log.info("Outbound message :" + message.toString());

        try {
            crack(message, sessionId);
        } catch (UnsupportedMessageType unsupportedMessageType) {
            unsupportedMessageType.printStackTrace();
        } catch (FieldNotFound fieldNotFound) {
            fieldNotFound.printStackTrace();
        } catch (IncorrectTagValue incorrectTagValue) {
            incorrectTagValue.printStackTrace();
        }
    }

    public void onMessage(quickfix.fix40.NewOrderSingle order, SessionID sessionID) throws FieldNotFound,
            UnsupportedMessageType, IncorrectTagValue {
        try {
            OrderQty orderQty = order.getOrderQty();
            Price price = order.getPrice();

            quickfix.fix40.ExecutionReport accept = new quickfix.fix40.ExecutionReport(genOrderID(), genExecID(),
                    new ExecTransType(ExecTransType.NEW), new OrdStatus(OrdStatus.NEW),
                    order.getSymbol(), order.getSide(), orderQty, new LastShares(0),
                    new LastPx(0), new CumQty(0), new AvgPx(0));

            accept.set(order.getClOrdID());
            sendMessage(sessionID, accept);

        } catch (RuntimeException e) {
            LogUtil.logThrowable(sessionID, e.getMessage(), e);
        }
    }

    private void sendMessage(SessionID sessionID, Message message) {
        try {
            Session session = Session.lookupSession(sessionID);
            if (session == null) {
                throw new SessionNotFound(sessionID.toString());
            }

            DataDictionaryProvider dataDictionaryProvider = session.getDataDictionaryProvider();
            if (dataDictionaryProvider != null) {
                try {
                    dataDictionaryProvider.getApplicationDataDictionary(
                            getApplVerID(session, message)).validate(message, true);
                } catch (Exception e) {
                    LogUtil.logThrowable(sessionID, "Outgoing message failed validation: "
                            + e.getMessage(), e);
                    return;
                }
            }

            session.send(message);
        } catch (SessionNotFound e) {
            e.printStackTrace();
        }
    }

    private ApplVerID getApplVerID(Session session, Message message) {
        String beginString = session.getSessionID().getBeginString();
        if (FixVersions.BEGINSTRING_FIXT11.equals(beginString)) {
            return new ApplVerID(ApplVerID.FIX40);
        } else {
            return MessageUtils.toApplVerID(beginString);
        }
    }


    public OrderID genOrderID() {
        return new OrderID(Integer.toString(++m_orderID));
    }

    public ExecID genExecID() {
        return new ExecID(Integer.toString(++m_execID));
    }

}
