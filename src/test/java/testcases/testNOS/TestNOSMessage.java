package testcases.testNOS;

import client.InSession;
import com.google.inject.Inject;
import common.SessionsModule;
import dataProviders.CommonDataProviders;
import exchange.ExchangeSession;
import exchange.OutSession;
import logging.common.TestListeners;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Guice;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import quickfix.Session;
import quickfix.SessionNotFound;
import quickfix.field.*;
import quickfix.fix40.NewOrderSingle;
import quickfix.fix42.ExecutionReport;

@Listeners(TestListeners.class)
@Guice(modules = SessionsModule.class)
@Test
public class TestNOSMessage {
    private static final Logger log = LoggerFactory.getLogger(TestNOSMessage.class);

    private InSession clientSession;
    private OutSession exchangeSession;

    private static int orderId = 0;
    private static int execId = 0;

    @Inject
    TestNOSMessage(InSession clientSession, OutSession exchangeSession){
        this.clientSession = clientSession;
        this.exchangeSession = exchangeSession;
    }

    @BeforeSuite
    public void setup() {
    }

    @Test(dataProvider = "symbol", dataProviderClass = CommonDataProviders.class)
    public void testNOS(String symbol) throws InterruptedException, SessionNotFound {
        NewOrderSingle nos = new NewOrderSingle(new ClOrdID("1"), new HandlInst('1'),
                new Symbol(symbol), new Side(Side.BUY), new OrderQty(100.0),
                new OrdType(OrdType.MARKET));
        nos.getHeader().setString(SenderCompID.FIELD, "Client");
        nos.getHeader().setString(TargetCompID.FIELD, "TradingApp");
        boolean sentNOS = Session.sendToTarget(nos, clientSession.getSessionID());

        ExecutionReport executionReport = new ExecutionReport(
                new OrderID(Integer.toString(++orderId)),
                new ExecID(Integer.toString(++execId)),
                new ExecTransType(ExecTransType.NEW),
                new ExecType(ExecType.NEW),
                new OrdStatus(OrdStatus.NEW),
                new Symbol("TCS.NS"),
                new Side(Side.BUY),
                new LeavesQty(),
                new CumQty(),
                new AvgPx());
        executionReport.setDouble(LastShares.FIELD, 0);
        executionReport.setDouble(AvgPx.FIELD, 0);
        executionReport.setDouble(LeavesQty.FIELD, 100);

        executionReport.getHeader().setString(SenderCompID.FIELD, "Server");
        executionReport.getHeader().setString(TargetCompID.FIELD, "TradingApp");
        boolean sentExecutionReport = Session.sendToTarget(executionReport, exchangeSession.getSessionID());

        log.info("Sent executionReport : " + sentExecutionReport + " and sentNos status is:" + sentNOS);
    }
}
