package logging.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestListener;
import org.testng.ITestResult;
import testcases.testNOS.TestNOSMessage;

public class TestListeners implements ITestListener {
    private static final Logger log = LoggerFactory.getLogger(TestListeners.class);

    @Override
    public void onTestStart(ITestResult result) {
        log.info("Test STARTED ****" + result.getName() + " **** ");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        log.info("Test PASSED ****" + result.getName() + " **** ");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        log.error("Test FAILED ****" + result.getName() + " **** ");
        log.error(result.getThrowable().getMessage());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        log.warn("Test SKIPPED ****" + result.getName() + " **** ");
    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        log.error("Test FAILED with Timeout ****" + result.getName() + " **** ");
        log.error(result.getThrowable().getMessage());
    }
}
